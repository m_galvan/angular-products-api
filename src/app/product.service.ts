import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'http://localhost:8000/api/';

  constructor(private httpClient: HttpClient) { }

  getProducts() {
    return this.httpClient.get(this.url + 'products');
  }

  getProduct(id: number) {
    return this.httpClient.get(this.url + 'product/' + id);
  }
}